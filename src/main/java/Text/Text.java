package Text;

import SentenceMap.SentenceTable;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {
    private final String FILE_PATH = "/home/oleg/home/oleg/Документи/IdeaProjects/Epam_String_Task/src/main/resources/text.txt";


    private StringBuilder stringBuilder = new StringBuilder();
    private FileInputStream fileInputStream = new FileInputStream(FILE_PATH);

    public Text() throws IOException {
    }

    private void getText() {
        int c;
        try {
            while ((c = fileInputStream.read()) != -1) {
                stringBuilder.append((char) c);
            }
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
        }

    }

    private void findTheMostCommon(int minLength) {
        int counter;
        int biggestCounter = 0;
        String theMostCommon = null;
        String patternString = "[A-z-']{" + minLength + ",}";

        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(stringBuilder);
        while (matcher.find()) {
            counter = 0;
//            String word = stringBuilder.substring(matcher.start(), matcher.end());
            String word = matcher.group();
            Pattern p = Pattern.compile(word);
            Matcher m = p.matcher(stringBuilder);
            while (m.find()) {
                counter++;
            }
            if (biggestCounter == 0) {
                biggestCounter = counter;
                theMostCommon = word;
            }
            if (biggestCounter < counter) {
                biggestCounter = counter;
                theMostCommon = word;
            }
        }
        System.out.println(theMostCommon + " " + biggestCounter + " times");
    }

    private void howMuchRepeats(String first,
                                String second,
                                String third,
                                String fourth,
                                String fifth) {

        int fCounter = 0;
        int sCounter = 0;
        int tCounter = 0;
        int foCounter = 0;
        int fiCounter = 0;
        Pattern pF = Pattern.compile(first);
        Matcher mF = pF.matcher(stringBuilder);
        while (mF.find()) {
            fCounter++;
        }
        System.out.println(first + " repeats " + fCounter + " times");
        Pattern pS = Pattern.compile(second);
        Matcher mS = pS.matcher(stringBuilder);
        while (mS.find()) {
            sCounter++;
        }
        System.out.println(second + " repeats " + sCounter + " times");
        Pattern pT = Pattern.compile(third);
        Matcher mT = pT.matcher(stringBuilder);
        while (mT.find()) {
            tCounter++;
        }
        System.out.println(third + " repeats " + tCounter + " times");
        Pattern pFo = Pattern.compile(fourth);
        Matcher mFo = pFo.matcher(stringBuilder);
        while (mFo.find()) {
            foCounter++;
        }
        System.out.println(fourth + " repeats " + foCounter + " times");
        Pattern pFi = Pattern.compile(fifth);
        Matcher mFI = pFi.matcher(stringBuilder);
        while (mFI.find()) {
            fiCounter++;
        }
        System.out.println(fifth + " repeats " + fiCounter + " times");

    }

    public void soutTheLongest() {
        int counter = 0;
        String sentence = "";

        String patternString = "[A-z-'!.?,]+";
        String patternEndOfSentence = "[!.?]+";

        SentenceTable sentenceTable = new SentenceTable();

        Pattern pattern = Pattern.compile(patternString);
        Pattern p = Pattern.compile(patternEndOfSentence);
        Matcher matcher = pattern.matcher(stringBuilder);
        while (matcher.find()) {

            counter++;
            String word = stringBuilder.substring(matcher.start(), matcher.end());
            sentence += (word + " ");
            Matcher m = p.matcher(word);

            if (m.find()) {
                sentenceTable.add(sentence);
                counter = 0;
                sentence = "";
            }
        }


    }


    public static void writeText(String string, String path) {

        final String OUT_FILE_PATH = "/home/oleg/home/oleg/Документи/IdeaProjects/Epam_String_Task/src/main/resources/test.txt";
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(path, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fileWriter.write(string);
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void fillSentenceTable(SentenceTable sentenceTable) {

        String sentence = "";
        String patternString = "[A-z-'!.?,]+";
        String patternEndOfSentence = "[!.?]+";

        Pattern pattern = Pattern.compile(patternString);
        Pattern p = Pattern.compile(patternEndOfSentence);
        Matcher matcher = pattern.matcher(stringBuilder);
        while (matcher.find()) {

            //String word = stringBuilder.substring(matcher.start(), matcher.end());
            String word = matcher.group();
            sentence += (word + " ");
            Matcher m = p.matcher(word);

            if (m.find()) {
                sentenceTable.add(sentence);
                sentence = "";
            }
        }

    }

    private void deleteAllConsonant(int length, SentenceTable sentenceTable) {
        final String OUT_TASK_12 = "/home/oleg/home/oleg/Документи/IdeaProjects/Epam_String_Task/src/main/resources/12.txt";
        StringBuilder newSB = new StringBuilder(stringBuilder);
        List<String> suitable = sentenceTable.findAllConsonant(length);
        for (String word :
                suitable) {
            Pattern p = Pattern.compile(word);
            Matcher m = p.matcher(newSB);
            while (m.find()) {
                newSB.delete(m.start(), m.end());
                m.reset();
            }
        }
        Text.writeText(newSB.toString(),OUT_TASK_12);

    }
    private void deleteRepeatsOfFirstLetter(){
        StringBuilder newSB = new StringBuilder();
        String wordPattern = "[A-z-',!?.]+\\s";
        Pattern p = Pattern.compile(wordPattern);
        Matcher wordM = p.matcher(stringBuilder);

        while(wordM.find()){
            StringBuilder word = new StringBuilder(wordM.group());
            boolean first = true;
            String firstLetterPattern = "["+String.valueOf(word.charAt(0)).toLowerCase()+"]";
            Pattern lp = Pattern.compile(firstLetterPattern);
            Matcher lm = lp.matcher(word);
            while (lm.find()){
                if (first){
                    first = false;
                    continue;
                }
                word.delete(lm.start(),lm.end());
                lm.reset();
            }
            newSB.append(word);
        }
        System.out.println(stringBuilder);
        System.out.println("///////////////////");
        System.out.println(newSB);

    }

    public void changeSubLine(int length, String sub){
        StringBuilder newSB = new StringBuilder(stringBuilder);
        String wordPattern = "[A-z-']+";
        Pattern p = Pattern.compile(wordPattern);
        Matcher m = p.matcher(newSB);
        while (m.find()){
            String word = m.group();
            if (word.length() == length) {
                newSB.replace(m.start(), m.end(), " " + sub);
                m.reset();
            }
        }
        System.out.println(newSB);
    }
    public void findPalindrome(){
        StringBuilder newSB = new StringBuilder(stringBuilder);
        String longest = "";
        for (int k = 3; k < 10 ; k++) {
            int startSearchAt = 0;
            String palindromePattern1 = "[A-z-']{"+k+"}";
            Pattern p1 = Pattern.compile(palindromePattern1);
            Matcher m1 = p1.matcher(newSB);

            while (m1.find(startSearchAt++)){
                StringBuilder subLine = new StringBuilder(m1.group());
                StringBuilder reversed = new StringBuilder(subLine).reverse();
                if (subLine.toString().equalsIgnoreCase(reversed.toString())){
                    //System.out.println(subLine);
                    if (subLine.length() > longest.length()){
                        longest = subLine.toString();
                    }
                }
            }
        }
        System.out.println("Longest palindrome : "+longest);

    }

    public void start() {
        SentenceTable sentenceTable = new SentenceTable();
        getText();
        fillSentenceTable(sentenceTable);

        System.out.println("3/////////Find unique from first sentence");
        sentenceTable.findUnique();

        System.out.println("1/The most common word");
        findTheMostCommon(2);

        System.out.println("7//////////Sort by percent of vowels");
        System.out.println(sentenceTable.sortByPercentOfVoulved());

        System.out.println("////////////////////////////////////Delete all Vowels");
        sentenceTable.DeleteAllThatBeginsOnVowel();

        System.out.println("9/////Sort by letter///////////////////////");
        sentenceTable.sortByLetter('m',true);

        System.out.println("8/Sort voulved");
        sentenceTable.sortWordsThatBeginsOnVoulved();

        System.out.println("10///////////How much repeats");
        howMuchRepeats("Audi", "Oleg", "is", "Hello", "Lil");

        System.out.println("5//////////////find and replace");
        sentenceTable.findAndReplace();

        System.out.println("4//////////Question sentence");
        sentenceTable.questionSentence(4);

        System.out.println("///////Test 11");
        sentenceTable.DeleteThatBeginsWithAndEndsWith("a", "e");

        System.out.println("///////test 12");
        //sentenceTable.findAllConsonant(6);
        deleteAllConsonant(5, sentenceTable);
        System.out.println("test 15////////////////////////");
        deleteRepeatsOfFirstLetter();
        System.out.println("/////test 16");
        changeSubLine(5,"1");
        System.out.println("////test 14");
        findPalindrome();
        System.out.println("///Test 13");
        sentenceTable.sortByLetter('l',false);
        System.out.println("/////Task #6");
        sentenceTable.sortAndPrintAllWords();

        System.out.println("2//////////Show sorted");
        sentenceTable.showSorted();

    }
}




