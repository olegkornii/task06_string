package SentenceMap;

import Text.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceTable {
    private List<String> sentenceList = new ArrayList<>();
    private List<String> wordList = new ArrayList<>();
    private String wordPattern = "[A-z-']+";

    public void add(String string) {
        sentenceList.add(string);
    }

    public void showSorted() {
        StringBuilder writeSorted = new StringBuilder();
        for (int i = 0; i < sentenceList.size(); i++) {
            for (int j = 0; j < sentenceList.size(); j++) {
                if (sentenceList.get(i).length() < sentenceList.get(j).length()) {
                    String tmp = sentenceList.get(j);
                    sentenceList.set(j, sentenceList.get(i));
                    sentenceList.set(i, tmp);
                }
            }
        }
        for (String s :
                sentenceList) {
            System.out.println(s);
            writeSorted.append(s).append("\n");
        }
        //Text.writeText(writeSorted.toString());
    }

    public void findUnique() {
        String word;
        boolean exist;
        exist = false;
        //String wordPattern = "[A-z-']+";
        Pattern pattern = Pattern.compile(wordPattern);
        Matcher matcher = pattern.matcher(sentenceList.get(0));
        while (matcher.find()) {
            word = sentenceList.get(0).substring(matcher.start(), matcher.end());
            exist = false;
            Pattern p = Pattern.compile(word);
            for (int i = 1; i < sentenceList.size(); i++) {
                Matcher m = p.matcher(sentenceList.get(i));
                if (m.find()) {
                    exist = true;
                }
            }
            if (!exist) {
                System.out.println("Word " + word + " exist only in first sentence");
            }
        }
    }

    public void questionSentence(int length) {

        String questionPattern = "[?]";
        Pattern pattern = Pattern.compile(questionPattern);
        Pattern p = Pattern.compile(wordPattern);
        List<String> questionSentences = new ArrayList<>();

        for (String sentence : sentenceList) {
            Matcher matcher = pattern.matcher(sentence);
            if (matcher.find()) {

                questionSentences.add(sentence);
            }

        }
        for (String sentence : questionSentences) {
            System.out.print(sentence + " --- ");
            Matcher m = p.matcher(sentence);
            List<String> wordsFromQuestion = new ArrayList<>();
            while (m.find()) {
                String word = sentence.substring(m.start(), m.end());

                if (wordsFromQuestion.contains(word)) continue;
                if (word.length() == length) {
                    System.out.print(word + " ");
                    wordsFromQuestion.add(word);
                }
            }
            System.out.println();
        }
    }

    public void findAndReplace() {

        final String OUT_TASK_5 = "/home/oleg/home/oleg/Документи/IdeaProjects/Epam_String_Task/src/main/resources/5.txt";
        String sentencePattern = "^[OAIEU]";
        // String wordPattern = "[A-z-']+";
        Pattern pattern = Pattern.compile(sentencePattern);
        StringBuilder builder = new StringBuilder();
        for (String aSentenceList : sentenceList) {
            Matcher matcher = pattern.matcher(aSentenceList);
            if (matcher.find()) {
                List<String> wordList = new ArrayList<>();
                Pattern p = Pattern.compile(wordPattern);
                Matcher m = p.matcher(aSentenceList);
                while (m.find()) {
                    // String word = aSentenceList.substring(m.start(), m.end());
                    String word = m.group();
                    wordList.add(word);
                }
                int indexOfTheLongestWord = 0;
                for (int k = 0; k < wordList.size(); k++) {
                    if (wordList.get(0).length() < wordList.get(k).length()
                            && k > indexOfTheLongestWord) {
                        indexOfTheLongestWord = k;
                    }
                }
                String tmp = wordList.get(0);
                String firstWord = wordList.get(0);
                String longestWord = wordList.get(indexOfTheLongestWord);
                String newSentence = aSentenceList.replaceAll(longestWord, firstWord).replaceFirst(firstWord, longestWord);
                //.replaceAll(firstWord,longestWord);
                //builder.append(aSentenceList.replaceAll(firstWord,longestWord));
                builder.append(newSentence);

                wordList.set(0, wordList.get(indexOfTheLongestWord));
                wordList.set(indexOfTheLongestWord, tmp);
                System.out.println(wordList);
                // System.out.println(builder);

            }
            Text.writeText(builder.toString(), OUT_TASK_5);
        }
    }

    public List<String> sortByPercentOfVoulved() {

        //  String wordPattern = "[A-z-']+";
        Pattern p = Pattern.compile(wordPattern);
        for (int i = 0; i < sentenceList.size(); i++) {
            Matcher m = p.matcher(sentenceList.get(i));
            while (m.find()) {
                //String word = sentenceList.get(i).substring(m.start(), m.end());
                String word = m.group();
                wordList.add(word);
            }
        }
        String vowelPattern = "[AEIOUaeiou]";
        Pattern vowelP = Pattern.compile(vowelPattern);
        for (int k = 0; k < wordList.size(); k++) {
            for (int i = 0; i < wordList.size() - 1; i++) {
                Matcher fVowelM = vowelP.matcher(wordList.get(i));
                int fCounter = 0;
                while (fVowelM.find()) {
                    fCounter++;
                }
                double fPercent = (double) fCounter / wordList.get(i).length();
                int j = i + 1;
                int sCounter = 0;
                Matcher sVowelM = vowelP.matcher(wordList.get(j));
                while (sVowelM.find()) {
                    sCounter++;
                }
                double sPercent = (double) sCounter / wordList.get(j).length();
                if (fPercent < sPercent) {
                    String tmp = wordList.get(j);
                    wordList.set(j, wordList.get(i));
                    wordList.set(i, tmp);
                }
            }
        }

        return wordList;
    }

    public void sortWordsThatBeginsOnVoulved() {
        List<String> voulwed = new ArrayList<>();
        String vowel = "^[AEIOUaeiou]";
        String consonant = "[qwrtypsdfghjklzxcvbnm]";
        Pattern p = Pattern.compile(vowel);
        for (String aWordList : wordList) {
            Matcher m = p.matcher(aWordList);
            if (m.find()) {
                voulwed.add(aWordList);
            }
        }
        Pattern pConsonant = Pattern.compile(consonant);
        for (int k = 0; k < voulwed.size(); k++) {
            for (int i = 0; i < voulwed.size() - 1; i++) {
                int j = i + 1;
                String firstWord = voulwed.get(i);
                String secondWord = voulwed.get(j);
                Matcher fMConsonant = pConsonant.matcher(firstWord);
                Matcher sMConsonant = pConsonant.matcher(secondWord);

                if (fMConsonant.find() && sMConsonant.find()) {
                    char fc = firstWord.charAt(fMConsonant.start());
                    char sc = secondWord.charAt(sMConsonant.start());
                    if (fc > sc) {
                        String tmp = firstWord;
                        voulwed.set(i, secondWord);
                        voulwed.set(j, tmp);
                    }
                }
            }
        }

        for (String str :
                voulwed) {
            System.out.print(str + " ");
        }
    }

    public void sortByLetter(char c, boolean fromBiggerToLower) {
        List<String> sorted = new ArrayList<>();
        String patternLetter = "[" + c + "]";
        Pattern p = Pattern.compile(patternLetter);
        for (String aWordList : wordList) {
            Matcher m = p.matcher(aWordList);
            if (m.find()) {
                sorted.add(aWordList);
            }
        }
        for (int k = 0; k < sorted.size(); k++) {
            for (int i = 0; i < sorted.size() - 1; i++) {
                int fCounter = 0;
                int sCounter = 0;
                Matcher m = p.matcher(sorted.get(i));
                while (m.find()) {
                    fCounter++;
                }
                int j = i + 1;
                Matcher sm = p.matcher(sorted.get(j));
                while (sm.find()) {
                    sCounter++;
                }
                if (fromBiggerToLower) {
                    if (fCounter < sCounter) {
                        String tmp = sorted.get(i);
                        sorted.set(i, sorted.get(j));
                        sorted.set(j, tmp);
                    }
                } else if (!fromBiggerToLower) {
                    if (fCounter > sCounter) {
                        String tmp = sorted.get(i);
                        sorted.set(i, sorted.get(j));
                        sorted.set(j, tmp);
                    }
                }
            }

        }
        System.out.println(sorted);
    }

    public void DeleteAllThatBeginsOnVowel() {
        String vowel = "^[^AEIOUaeiou]";
        StringBuilder sb = new StringBuilder();
        Pattern p = Pattern.compile(vowel);
        for (String aWordList : wordList) {
            Matcher m = p.matcher(aWordList);
            if (m.find()) {
                System.out.print(aWordList + " ");
            }
        }

        System.out.println(sb);
    }

    //*
    public void DeleteThatBeginsWithAndEndsWith(String begin, String end) {
        final String OUT_TASK_11 = "/home/oleg/home/oleg/Документи/IdeaProjects/Epam_String_Task/src/main/resources/11.txt";
        String patternString = begin + "+[A-z-']+" + end;
        StringBuilder sb = new StringBuilder();
        Pattern p = Pattern.compile(patternString);
        for (String sentence : sentenceList) {
            Matcher m = p.matcher(sentence);
            String newSentence = "";
            while (m.find()) {
                String[] splitString = sentence.split(sentence.substring(m.start(), m.end()));
                newSentence = splitString[0].concat(splitString[1]);
                sb.append(newSentence);
                System.out.println(newSentence);
            }
        }
        Text.writeText(sb.toString(), OUT_TASK_11);
    }

    public List<String> findAllConsonant(int length) {
        String stringPattern = "^[^AEIOUaeiou]+[A-z-']{" + (length - 1) + "}";
        List<String> allSuitable = new ArrayList<>();
        Pattern p = Pattern.compile(stringPattern);
        for (String word :
                wordList) {
            Matcher m = p.matcher(word);
            if (m.find()) {
                allSuitable.add(word);
            }
        }
        System.out.println(allSuitable);
        return allSuitable;
    }

    public void sortAndPrintAllWords() {
        wordList.sort(String::compareTo);
        //System.out.print(wordList.get(0));
        StringBuilder sb = new StringBuilder(wordList.get(0));
        for (int i = 1; i < wordList.size(); i++) {
            String word = wordList.get(i);
            char firstOfWord = word.charAt(0);
            char firstOfPreviousWord = wordList.get(i - 1).charAt(0);
            if (firstOfWord != firstOfPreviousWord) {
                sb.append("\n   ").append(word);
            } else {
                sb.append(" ").append(word);
            }
        }
        System.out.println(sb);
    }
}